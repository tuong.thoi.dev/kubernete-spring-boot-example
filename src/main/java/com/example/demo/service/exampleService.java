package com.example.demo.service;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class exampleService {
    @GetMapping("/")
    public ResponseEntity<String> getApplication(){
        return ResponseEntity.ok("Running !!!");
    }
    @GetMapping("/testing")
    public ResponseEntity<String> getTestApplication(){
        return ResponseEntity.ok("Running test!!!");
    }
}
